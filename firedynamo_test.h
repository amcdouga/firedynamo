// -----------------------------------------------------------------------------
//
// Copyright (C) 2021 CERN & Newcastle University for the benefit of the
// BioDynaMo collaboration. All Rights Reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
//
// See the LICENSE file distributed with this work for details.
// See the NOTICE file distributed with this work for additional information
// regarding copyright ownership.
//
// -----------------------------------------------------------------------------
#ifndef FIREDYNAMO_TEST_H_
#define FIREDYNAMO_TEST_H_

#include "biodynamo.h"

namespace bdm {

// Define my custom cell Plant, which extends Cell by adding extra data
// members: plant_state
class Fuel : public Cell {  // our object extends the Cell object
                              // create the header with our new data member
                              // I replaced MyCell with Fuel for firedynamo
  BDM_AGENT_HEADER(Fuel, Cell, 1);

 public:
  Fuel() {}
  explicit Fuel(const Double3& position) : Base(position) {}
  virtual ~Fuel() {}

  /// If Fuel divides, the daughter has to initialize its attributes
  //void Initialize(const NewAgentEvent& event) override {
  //  Base::Initialize(event);

    //if (auto* mother = dynamic_cast<Plant*>(event.existing_agent)) {
    //  cell_color_ = mother->cell_color_;
    // if (event.GetUid() == CellDivisionEvent::kUid) {
        // the daughter will be able to divide
     //   can_divide_ = true;
      //} else {
     //   can_divide_ = mother->can_divide_;
    //  }
   // }
  //}

  // getter and setter for our new data member
  //void SetCanDivide(bool d) { can_divide_ = d; }
  //bool GetCanDivide() const { return can_divide_; }

  void SetFuelState(int fuel_state) { fuel_state_ = fuel_state; }
  int GetFuelState() const { return fuel_state_; }

 private:
  // declare new data member and define their type
  // private data can only be accessed by public function and not directly
 // bool can_divide_;
  int fuel_state_; //0: unburnt fuel, 1: not flammable, 2: burning, 3: burnt
};


inline int Simulate(int argc, const char** argv) {
  auto set_param = [](Param* param) {
    param->bound_space = true;
    param->min_bound = 0;
    param->max_bound = 100;  // cube of 100*100*100
  };

  Simulation simulation(argc, argv, set_param);
  // Define initial model
  auto* rm = simulation.GetResourceManager();
  auto* param = simulation.GetParam();
  auto* myrand = simulation.GetRandom();

  size_t nb_of_fuels = 10;  // number of cells in the simulation
  double x_coord, y_coord, z_coord;

  for (size_t i = 0; i < nb_of_fuels; ++i) {
    // our modelling will be a cell cube of 100*100*100
    // random double between 0 and 100
    x_coord = myrand->Uniform(param->min_bound, param->max_bound);
    y_coord = myrand->Uniform(param->min_bound, param->max_bound);
    z_coord = 0; // we limit ourselves to 2D for now, else: myrand->Uniform(param->min_bound, param->max_bound);

    // creating the cell at position x, y, z
    Fuel* fuel = new Fuel({x_coord, y_coord, z_coord});
    // set cell parameters
    fuel->SetDiameter(7.5);
    // will vary from 0 to 5. so 6 different layers depending on y_coord
    fuel->SetFuelState(static_cast<int>((y_coordinate /max_bound *6)))); //make the plant either a non-burned plant or a non-flammable fuel

    rm->AddAgent(fuel);  // put the created cell in our cells structure
  }
  // create a cancerous cell, containing the behavior Growth
  //MyCell* cell = new MyCell({20, 50, 50});
  //cell->SetDiameter(6);
  //cell->SetCellColor(8);
  //cell->SetCanDivide(true);
  //cell->AddBehavior(new Growth());
  //rm->AddAgent(cell);  // put the created cell in our cells structure

  // Run simulation for .. number of steps
  simulation.GetScheduler()->Simulate(10);

  std::cout << "Simulation completed successfully!" << std::endl;
  return 0;
}

}  // namespace bdm

#endif  // FIREDYNAMO_TEST_H_
