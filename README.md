# FireDynamo

Development of code for FireDynamo project

For use inside the BioDynamo software: https://biodynamo.org

To run: `git clone https://gitlab.cern.ch/amcdouga/firedynamo.git`
